#include "test.h"

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <sys/mman.h>
#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x100000
#endif

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void debug(const char* fmt, ... );

void* init_heap() {
    debug("Init heap:\n");
    void* heap = heap_init(8175);
    if (heap == NULL){
        err("Failed. Heap allocator broken.\n");
    }
    debug_heap(stdout, heap);
    debug("Passed\n");
    return heap;
}

void test1(void* heap) {
    debug("Test 1:\n");
    void* block1 = _malloc(50);
    if (!block1){
        err("Failed. Single block can not be allocate.\n");
    }
    debug_heap(stdout, heap);
    debug("Passed\n");
    _free(block1);
}

void test2(void* heap) {
    debug("Test 2:\n");
    void* block1 = _malloc(50);
    _free(block1);
    struct block_header* header = block_get_header(block1);
    if (!(header -> is_free)){
        err("Failed. Block not freed.\n");
    }
    debug_heap(stdout, heap);
    debug("Passed\n");
}

void test3(void* heap) {
    debug("Test 3:\n");
    void* block1 = _malloc(50);
    void* block2 = _malloc(70);
    _free(block1);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    if (!header1 -> is_free || header2 -> is_free){
        err("Failed. Allocation and freeing broken.\n");
    }   
    debug_heap(stdout, heap);
    debug("Passed\n");
    _free(block2); 
}

void test4(void* heap) {
    debug("Test 4:\n");
    void* block1 = _malloc(8175);
    void* block2 = _malloc(2048);
    if (!block2){
        err("Failed. Heap grown failed.\n");
    }
    debug_heap(stdout, heap);
    debug("Passed\n");
    _free(block1);
    _free(block2);
}

void test5(void* heap) {
    debug("Test 5:\n");
    struct block_header* first_block = (struct block_header*) heap;
    void* block1 = _malloc(20000);
    if (!block1){
        err("Failed. Block equals NULL.\n");
    }
    struct block_header *start = first_block;
    while (start->next != NULL) start = start->next;
    map_pages((uint8_t *) start + size_from_capacity(start->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void* block2 = _malloc(40000);
    if (block_get_header(block2) == start){
        err("Failed. Heap grown failed.\n");
    }
    debug_heap(stdout, heap);
    debug("Passed\n");
    _free(block1);
    _free(block2);
}
