#include "test.h"

int main() {
    void* heap = init_heap();
    test1(heap);
    test2(heap);
    test3(heap);
    test4(heap);
    test5(heap);
    return 0;
}
